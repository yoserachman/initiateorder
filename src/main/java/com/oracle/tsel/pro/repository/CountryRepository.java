package com.oracle.tsel.pro.repository;

import java.util.List;

import javax.enterprise.context.ApplicationScoped;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import com.oracle.tsel.pro.domain.Country;

@ApplicationScoped
public class CountryRepository {
	
	@PersistenceContext
	private EntityManager entityManager;
	
	public List<Country> findAll(){
		return this.entityManager.createNamedQuery("Country.findAll").getResultList();
	}
	
	public Country findById(String id){
		return this.entityManager.find(Country.class, id);
	}
}
