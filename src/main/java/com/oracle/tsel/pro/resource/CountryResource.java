package com.oracle.tsel.pro.resource;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.oracle.tsel.pro.service.CountryService;

@Path("/country")
@RequestScoped
public class CountryResource {

	private CountryService countryService;
	
	@Inject
	public CountryResource(CountryService countryService) {
		this.countryService = countryService;
	}
	
	@GET
	@Path("")
    @Produces(MediaType.APPLICATION_JSON)
    public Response findAll() {
        return Response.ok(countryService.findAll()).build();
    }
	
	@GET
	@Path("{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response findOne(@PathParam("id") String id) {
        return Response.ok(countryService.findById(id)).build();
    }
}
