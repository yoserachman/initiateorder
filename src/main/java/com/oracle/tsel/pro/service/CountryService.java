package com.oracle.tsel.pro.service;

import java.util.List;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

import com.oracle.tsel.pro.domain.Country;
import com.oracle.tsel.pro.repository.CountryRepository;

@ApplicationScoped
public class CountryService {
	
	private CountryRepository countryRepository;

	@Inject
	public CountryService(CountryRepository countryRepository) {
		this.countryRepository = countryRepository;
	}
	
	public List<Country> findAll(){
		return this.countryRepository.findAll();
	}
	
	public Country findById(String id) {
		return this.countryRepository.findById(id);
	}
}
