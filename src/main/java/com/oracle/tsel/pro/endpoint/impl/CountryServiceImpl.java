package com.oracle.tsel.pro.endpoint.impl;

import java.rmi.RemoteException;
import java.util.List;

import javax.annotation.Resource;
import javax.enterprise.context.spi.CreationalContext;
import javax.enterprise.inject.spi.Bean;
import javax.enterprise.inject.spi.BeanManager;
import javax.enterprise.inject.spi.CDI;
import javax.jws.WebService;
import javax.xml.ws.WebServiceContext;

import com.oracle.tsel.pro.domain.Country;
import com.oracle.tsel.pro.endpoint.CountryServicePortType;
import com.oracle.tsel.pro.service.CountryService;


@WebService(endpointInterface = "com.oracle.tsel.pro.endpoint.CountryServicePortType")
public class CountryServiceImpl implements CountryServicePortType {
	
	@Resource
	private WebServiceContext wsContext;
	
	protected CountryService getCountryService() {
		return CDI.current().select(CountryService.class).get();
	}

	@Override
	public Country[] findAll() throws RemoteException {	
		List<Country> results = getCountryService().findAll();
		return results.size()>0?results.toArray(new Country[results.size()]):null;
	}

	@Override
	public Country findOne(String id) throws RemoteException {
		return getCountryService().findById(id);
	}

}
