package com.oracle.tsel.pro.endpoint;

import java.rmi.Remote;
import java.rmi.RemoteException;

import javax.jws.WebMethod;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import javax.jws.soap.SOAPBinding.ParameterStyle;
import javax.jws.soap.SOAPBinding.Style;
import javax.jws.soap.SOAPBinding.Use;

import com.oracle.tsel.pro.domain.Country;

@WebService(portName = "CountryServicePortType", targetNamespace = "http://www.telkomsel.co.id/InitOrder")
@SOAPBinding(parameterStyle = ParameterStyle.BARE, style = Style.DOCUMENT, use = Use.LITERAL)
public interface CountryServicePortType extends Remote {
	
	@WebMethod(operationName = "findAll")
	@WebResult(name = "return")
	public Country[] findAll() throws RemoteException;
	
	@WebMethod(operationName = "findOne")
	@WebResult(name = "country")
	public Country findOne(String id) throws RemoteException;

}
